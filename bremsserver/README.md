Bremsserver
===========

Implementation of a 

- http - server
- mail - server
- proxy -server

for web development and testing. Implementation provides the following features:
- shows network traffic and http headers
- slows down response to simulate slow network (configurabl)
- set no caching headers (configurable)

Please report bugs to the [issue tracker](https://bitbucket.org/herberlin/bremsserver/issues).


![Screenshot](./support/Screenshot.png)