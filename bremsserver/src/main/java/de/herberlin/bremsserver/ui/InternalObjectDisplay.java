package de.herberlin.bremsserver.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.text.html.HTMLDocument;

import de.herberlin.server.common.event.FileData;
import de.herberlin.server.common.event.MailRequestEvent;
import de.herberlin.server.common.event.TimeData;
import de.herberlin.server.mail.MailHeader;
import de.herberlin.wwwutil.RequestHeader;
import de.herberlin.wwwutil.ResponseHeader;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 12.02.2003
 */
public class InternalObjectDisplay extends JInternalFrame {

	public InternalObjectDisplay() {
		super();
		setSize(50, 100);
		setVisible(false);
		setResizable(true);
		setMaximizable(true);
		setIconifiable(false);
		setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
		setClosable(true);
	}

	public void setObject(Object displayObject) {
		this.getContentPane().removeAll();
		if (displayObject instanceof RequestHeader) {
			makeRequestDisplay((RequestHeader) displayObject);
		} else if (displayObject instanceof ResponseHeader) {
			makeResponseDisplay((ResponseHeader) displayObject);
		} else if (displayObject instanceof TimeData) {
			makeTimeDisplay((TimeData) displayObject);
		} else if (displayObject instanceof FileData) {
			makeFileDisplay((FileData) displayObject);
		}
		this.moveToFront();
		show();
		revalidate();
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}

	private void makeFileDisplay(FileData fileData) {

		setTitle("File");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		JComponent displayable = null;

		if (fileData != null && fileData.getInputStream() != null) {

			c.add(new JLabel(fileData.toString()), BorderLayout.NORTH);

			if (fileData.getContentType() != null && fileData.toString().indexOf("image") > -1) {

				// show image
				JLabel label = new JLabel(new ImageIcon(fileData.getBytes()));
				displayable = label;

			} else {

				// show content as text
				JTextArea edit = new JTextArea();
				edit.setEditable(true);
				edit.setFont(new Font("Monospaced", Font.PLAIN, 12));
				try {
					InputStreamReader in;

					if ("gzip".equalsIgnoreCase(fileData.getEncoding())) {
						in = new InputStreamReader(new GZIPInputStream(fileData.getInputStream()));
					} else {

						in = new InputStreamReader(fileData.getInputStream());
					}
					edit.read(in, new HTMLDocument());
					displayable = edit;
				} catch (IOException io) {
					logger.fine("File not readable: " + io);
				}
			}

			c.add(new JLabel(fileData.getContentDescription()), BorderLayout.SOUTH);
		} else {
			// fileData or InputStream == null; cant display
			new JLabel("Unable to display.", JLabel.CENTER);
		}
		c.add(new JScrollPane(displayable));
	}

	private Logger logger = Logger.getLogger(getClass().getName());

	private void makeTimeDisplay(TimeData timeData) {
		setTitle("Time");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		c.add(new JLabel(), BorderLayout.NORTH);
		c.add(new JScrollPane(new JTable(timeData.getDisplayValues(), new String[] { "Key", "Value" })),
				BorderLayout.CENTER);
		c.add(new JLabel(timeData.getURL()), BorderLayout.SOUTH);
	}

	private void makeResponseDisplay(ResponseHeader response) {
		setTitle("Response");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		c.add(new JLabel(), BorderLayout.NORTH);
		c.add(new JScrollPane(new JTable(response.getAllHeaders(), new String[] { "Key", "Value" })),
				BorderLayout.CENTER);
		c.add(new JLabel(response.getFirstLine()), BorderLayout.SOUTH);
	}

	private void makeRequestDisplay(RequestHeader request) {
		setTitle("Request");
		Container c = getContentPane();
		c.setLayout(new BorderLayout());
		c.add(new JLabel(), BorderLayout.NORTH);
		JScrollPane headerTable = new JScrollPane(new JTable(request.getAllHeaders(), new String[] { "Key", "Value" }));

		if (request.getPostData() == null) {
			c.add(headerTable, BorderLayout.CENTER);
		} else {
			JTextArea postDataText = new JTextArea(new String(request.getPostData()));
			postDataText.setLineWrap(true);
			JScrollPane postDataTable = new JScrollPane(postDataText);
			JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
			split.setTopComponent(headerTable);
			split.setBottomComponent(postDataTable);
			split.setResizeWeight(.7);
			c.add(split, BorderLayout.CENTER);
		}
		c.add(new JLabel(request.getFirstLine()), BorderLayout.SOUTH);
	}

	public void makeMailDisplay(LogTableModel model, int rowIndex, int colIndex) {
		MailRequestEvent event = (MailRequestEvent) model.getCurrentEvent(rowIndex);
		MailHeader header = event.getHeaders();
		setTitle("Mail");
		
		Container c = getContentPane();
		c.removeAll();
		c.setLayout(new BorderLayout());
		c.add(new JLabel(), BorderLayout.NORTH);
		JScrollPane headerTable = new JScrollPane(new JTable(header.getHeader(), new String[] { "Key", "Value" }));

		String displayContent = colIndex < 2 ? header.getContent() : event.getConversation();

		c.add(headerTable, BorderLayout.CENTER);
		JTextArea postDataText = new JTextArea(displayContent);
		postDataText.setLineWrap(true);
		JScrollPane postDataTable = new JScrollPane(postDataText);
		JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		split.setTopComponent(headerTable);
		split.setBottomComponent(postDataTable);
		split.setResizeWeight(.7);
		c.add(split, BorderLayout.CENTER);

		this.moveToFront();
		show();
		revalidate();
		try {
			setSelected(true);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
	}
}
