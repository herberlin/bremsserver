package de.herberlin.bremsserver.ui;

import java.awt.BorderLayout;
import java.net.URL;

import javax.swing.JFrame;

import de.herberlin.bremsserver.Main;
import de.herberlin.bremsserver.config.Configuration;
import de.herberlin.helpsystem.SimpleHelpSystem;

/**
 * @author hans joachim herbertz created 04.03.2004
 */
public abstract class Help {

	private static SimpleHelpSystem helpSystem = null;
	public static JFrame frame = null;

	public static void show(String path) {
		if (helpSystem == null) {
			URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
			helpSystem = new SimpleHelpSystem(url.getFile());
			frame = new JFrame(Configuration.getVersion()+ " - Help");
			frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			frame.getContentPane().setLayout(new BorderLayout());
			frame.getContentPane().add(helpSystem);
			frame.setSize(320, 320);
		}
		frame.setVisible(true);
		helpSystem.display(path);

	}
}
