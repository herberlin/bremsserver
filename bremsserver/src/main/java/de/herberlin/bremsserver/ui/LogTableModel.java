package de.herberlin.bremsserver.ui;

import java.util.LinkedList;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import de.herberlin.bremsserver.config.ConfigConstants;
import de.herberlin.bremsserver.config.Configuration;
import de.herberlin.server.common.event.EventDispatcher;
import de.herberlin.server.common.event.RequestEvent;
import de.herberlin.server.common.event.ServerEvent;
import de.herberlin.server.common.event.ServerEventListener;
import de.herberlin.server.common.event.ServerStartEvent;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 08.02.2003
 */
public class LogTableModel extends AbstractTableModel implements ServerEventListener {

	private LinkedList dataList = new LinkedList();

	private Object[] columnNames = new String[] { "Time", "Request", "Response", "Content" };

	/**
	 * Constructor for LogTableModel.
	 */
	private LogTableModel() {
		super();
	}

	public String getColumnName(int column) {
		if (columnNames != null && columnNames.length > column) {
			return (String) columnNames[column];
		} else {
			return "";
		}
	}

	public void add(ServerEvent event) {

		if (event instanceof ServerStartEvent) {

			this.columnNames = ((ServerStartEvent) event).getColumnNames();
			dataList = new LinkedList();
			fireTableStructureChanged();

		} else if (event instanceof RequestEvent) {

			// display the event

			dataList.addFirst(event);
			if (dataList.size() > Configuration.getPrefs().getInt(ConfigConstants.LOGSIZE, 100)) {
				dataList.removeLast();
			}
			fireTableRowsInserted(0, 1);
		}
	}

	/**
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() {
		return dataList.size();
	}

	/**
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() {
		return 4;
	}

	/**
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= dataList.size())
			return null;
		if (columnIndex >= 4)
			return null;
		ServerEvent event = (ServerEvent) dataList.get(rowIndex);
		return event.getValueAt(columnIndex);
	}
	
	public ServerEvent getCurrentEvent(int rowIndex) {
		return (ServerEvent) dataList.get(rowIndex);
	}

	@Override
	public void onServerEvent(ServerEvent ev) {
		add(ev);
	}

	private static LogTableModel _instance = null;

	public static TableModel getModel() {
		if (_instance == null) {
			_instance = new LogTableModel();
			EventDispatcher.addServerEventListener(_instance);
		}
		return _instance;
	}

}
