package de.herberlin.bremsserver;

import java.awt.Desktop;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.logging.LogManager;

import javax.swing.UIManager;

import de.herberlin.bremsserver.config.ConfigConstants;
import de.herberlin.bremsserver.config.Configuration;
import de.herberlin.bremsserver.config.impl.HttpConfigurationImpl;
import de.herberlin.bremsserver.ui.MainWindow;
import de.herberlin.server.httpd.HttpServer;
import de.herberlin.server.httpd.HttpServerConfig.Factory;
import de.herberlin.server.mail.MailServer;

/**
 * Hello world!
 *
 */
public class Main {

	public static void main(String[] args) {

		print(Configuration.getVersion());

		initLogging();
		int error = 0;
		boolean ui = false;

		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-m":
				int port = getPort(args, i);
				if (port != -1) {
					Configuration.setPort(ConfigConstants.MODE_MAIL, port);
					i++;
				}
				break;

			case "-p":
				port = getPort(args, i);
				if (port != -1) {
					Configuration.setPort(ConfigConstants.MODE_HTTP, port);
					i++;
				}
				break;
			case "-ui":
				ui = true;
				break;
			case "-h":
				error = 1;
				break;
			case "--help":
				error = 1;
				break;
			default:
				print("Invalid parameter: " + args[i]);
				print("Program will exit.");
				error = 1;
				break;
			}
		}
		if (error == 0) {
			Factory.setConfig(new HttpConfigurationImpl());

			if (ui || forceUi()) {
				setLookAndFeel();
				new MainWindow().setVisible(true);
			} else {
				print("Starting mailserver at port: " + Configuration.getPort(ConfigConstants.MODE_MAIL));
				print("Starting httpserver at port: " + Configuration.getPort(ConfigConstants.MODE_HTTP));
				unzipDocs();
				launchBrowser();
				new HttpServer().start(Configuration.getPort(ConfigConstants.MODE_HTTP));
				new MailServer().start(Configuration.getPort(ConfigConstants.MODE_MAIL));
			}
		} else {
			printHelp();
		}
	}

	private static void unzipDocs() {
		try {
			URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
			String filename = url.getFile().replace(url.getProtocol() + ":", "");
			new Unzip(filename, "docroot").unzipTo(Configuration.getDocroot());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void launchBrowser() {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			try {
				Desktop.getDesktop().browse(new URI(
						"http://localhost:" + Configuration.getPort(ConfigConstants.MODE_HTTP) + "/index.html"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private static boolean forceUi() {
		return System.console() == null;
		// return GraphicsEnvironment.isHeadless();
	}

	private static void printHelp() {
		print("-m mailserver port");
		print("-p http-server port");
		print("-ui start graphical use interface.");
		print("-h this help.");
		print("---");
		print("Mailserver port set to: " + Configuration.getPort(ConfigConstants.MODE_MAIL));
		print("Httpserver port set to: " + Configuration.getPort(ConfigConstants.MODE_HTTP));

	}

	private static int getPort(String[] args, int i) {
		int result = -1;
		if (i + 1 < args.length) {
			try {
				result = Integer.parseInt(args[i + 1]);
			} catch (Exception e) {
				print("Error: Can't parse " + args[i + 1] + " to port number.");
			}
		} else {
			print("Missing value for " + args[i]);
		}
		return result;
	}

	private static void print(String t) {
		System.out.println(t);

	}

	private static void setLookAndFeel() {
		try {
			// UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	private static void initLogging() {

		// read logfile
		InputStream in = MainWindow.class.getResourceAsStream("/logging.properties");
		if (in != null) {
			try {
				LogManager.getLogManager().readConfiguration(in);
			} catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				if (in != null) {
					try {
						in.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
