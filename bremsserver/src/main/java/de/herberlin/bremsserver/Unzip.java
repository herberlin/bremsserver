package de.herberlin.bremsserver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Unzip {

	private ZipFile zipfile = null;
	private String pattern = null;

	public Unzip(String zipfile, String subdir) throws IOException {
		this.zipfile = new ZipFile(zipfile);
		if (subdir.startsWith("/")) {
			subdir = subdir.substring(1);
		}
		this.pattern = subdir;
	}

	private File root= null; 
	public void unzipTo(String root) throws Exception {
		this.root = new File(root);
		if (!this.root.exists()) {
			this.root.mkdirs();
		}
		System.out.println("Creating docroot at: " + this.root);
		zipfile.stream()
			.filter(entry -> entry.getName().startsWith(pattern))
			.forEach(entry -> makeEntry(entry));
		
	}
	
	private void makeEntry(ZipEntry entry)  {
		String entryName = entry.getName().replaceFirst(pattern, "");
		File file = new File(root,entryName);
		// System.out.println(file);
		if (entry.isDirectory()) {
			file.mkdirs();
		} else {
			if (!file.exists()) {
				try {
					Files.copy(zipfile.getInputStream(entry), file.toPath());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
