package de.herberlin.bremsserver;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.junit.Test;

import de.herberlin.bremsserver.config.ConfigConstants;
import de.herberlin.bremsserver.config.Configuration;

public class JustSendMails {


	@Test
	public void testSmallMail() throws Exception {
		MimeMessage message = getMessage();
		String content = "Yep this is the text";
		message.setText(content);
		sendMessage(message);
	}

	String longText = "Vor dem Aufkommen von E-Mail wurden Nachrichten als Brief oder Telegramm, sp\u00e4ter auch - als die ersten beiden elektronischen \u00dcbertragungsverfahren - Fernschreiben (Telex) und Teletex sowie Fax \u00fcbermittelt. Ende der 1980er Jahre begann dann die weltweite Verbreitung der E-Mail - sie war eine der ersten Anwendungen, die die M\u00f6glichkeiten des Arpanets nutzten.\n"
			+ "Die Einf\u00fchrung von E-Mail wurde nicht gezielt vorangetrieben, sondern eroberte das Netzwerk wegen des Benutzerverhaltens. Das \u00fcberraschte die Arpanet-Initiatoren, denn noch 1967 hatte Lawrence Roberts, der sp\u00e4tere Leiter von IPTO, gesagt, die M\u00f6glichkeit des Austausches von Botschaften unter den Netzwerkteilnehmern sei kein wichtiger Beweggrund, um ein Netzwerk von wissenschaftlichen Rechnern aufzubauen (\"not an important motivation for a network of scientific computers\").\n"
			+ "\n"
			+ "Ein Vorl\u00e4ufer der E-Mail war das MAIL-Systemkommando in der Erweiterung Multics des CTSS Time-Sharing-Systems am MIT, vorgeschlagen 1964/65 von den Systementwicklern Glenda Schroeder, Louis Pouzin und Pat Crisman und implementiert 1965 von Tom Van Vleck.[8][9] M\u00f6glichkeiten, Mail im Arpanet zu versenden, regte J. C. R. Licklider schon 1968 an und die Idee wurde unter den Entwicklern diskutiert (RFC 196, \"Mail Box Protocol\" von Richard W. Watson vom 20. Juli 1971). Nachdem Multics, in dem ein Mail-Programm zur Kommunikation der Nutzer implementiert worden war, im Oktober 1971 an das Arpanet angeschlossen worden war, wurde Anfang 1972 ein Mail-Programm \u00fcber das Arpanet von der MAC Networking Group unter Mike Padlipsky implementiert.\n"
			+ "\n"
			+ "Ray Tomlinson hat im Jahr 1971 den ersten elektronischen Brief verschickt und gilt seitdem als Erfinder der E-Mail.[10] Er war bei dem Forschungsunternehmen Bolt, Beranek and Newman (BBN) an der Entwicklung des Betriebssystems TENEX beteiligt, das auf vielen im Arpanet verbundenen Rechnern zur Verf\u00fcgung stand, und besch\u00e4ftigte sich dabei unter anderem mit dem Programm SNDMSG f\u00fcr die \u00dcbermittlung von Nachrichten unter den Benutzern des Gro\u00dfrechners und dem Protokoll CPYNET f\u00fcr die \u00dcbertragung von Dateien zwischen Computern.[11] Programme wie SNDMSG gab es wie erw\u00e4hnt bereits seit den fr\u00fchen 1960er Jahren. Sie erm\u00f6glichten Benutzern, den Mailboxen anderer Benutzer desselben Computers Text hinzuzuf\u00fcgen. Eine Mailbox war seinerzeit nichts weiter als eine einzelne Datei, die nur ein Benutzer lesen konnte. Tomlinson kam 1971 auf die Idee, CPYNET so zu \u00e4ndern, dass es vorhandene Dateien erg\u00e4nzen konnte und es dann in SNDMSG einzuarbeiten.[11] Die erste Anwendung dieser Kombination war eine Nachricht von Tomlinson an seine Kollegen, in der er Ende 1971 mitteilte, dass man nun Nachrichten \u00fcbers Netzwerk senden konnte, indem man dem Benutzernamen des Adressaten das Zeichen \"@\" und den Hostname des Computers anf\u00fcgte.";

	@Test
	public void testLargeMailWithUmlauts() throws Exception {
		MimeMessage message = getMessage();

		message.setText(longText, "ISO-8859-1");
		sendMessage(message);
	}

	@Test
	public void testHtml() throws Exception {
		MimeMessage message = getMessage();
		String content = "<p>Die <b>Hypertext Markup Language</b> (<b>HTML</b>, <a href=\"/wiki/Englische_Sprache\" title=\"Englische Sprache\">englisch</a> für <i><a href=\"/wiki/Hypertext\" title=\"Hypertext\">Hypertext</a>-Auszeichnungssprache</i>) ist eine textbasierte <a href=\"/wiki/Auszeichnungssprache\" title=\"Auszeichnungssprache\">Auszeichnungssprache</a> zur Strukturierung <a href=\"/wiki/Elektronisches_Dokument\" title=\"Elektronisches Dokument\">elektronischer Dokumente</a> wie <a href=\"/wiki/Textdatei\" title=\"Textdatei\">Texte</a> mit <a href=\"/wiki/Hyperlink\" title=\"Hyperlink\">Hyperlinks</a>, <a href=\"/wiki/Bilddatei\" title=\"Bilddatei\">Bildern</a> und anderen Inhalten. <a href=\"/wiki/Webseite\" title=\"Webseite\">HTML-Dokumente</a> sind die Grundlage des <a href=\"/wiki/World_Wide_Web\" title=\"World Wide Web\">World Wide Web</a> und werden von <a href=\"/wiki/Webbrowser\" title=\"Webbrowser\">Webbrowsern</a> dargestellt. Neben den vom Browser angezeigten Inhalten können HTML-Dateien zusätzliche Angaben in Form von <a href=\"/wiki/Metadaten\" title=\"Metadaten\">Metainformationen</a> enthalten, z.&nbsp;B. über die im Text verwendeten <a href=\"/wiki/Einzelsprache\" title=\"Einzelsprache\">Sprachen</a>, den <a href=\"/wiki/Autor\" title=\"Autor\">Autor</a> oder den zusammengefassten Inhalt des Textes.\n"
				+ "</p>";
		message.setContent(content, "text/html");
		sendMessage(message);
	}

	@Test
	public void testMailWithAttachment() throws Exception {
		MimeMessage message = getMessage();

		BodyPart messageBodyPart1 = new MimeBodyPart();
		messageBodyPart1.setText("This is message body");

		MimeBodyPart messageBodyPart2 = new MimeBodyPart();
		String filename = "about-mail.txt";
		DataSource source = new ByteArrayDataSource(longText, "text/plain");
		messageBodyPart2.setDataHandler(new DataHandler(source));
		messageBodyPart2.setFileName(filename);

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart1);
		multipart.addBodyPart(messageBodyPart2);
		message.setContent(multipart);

		sendMessage(message);
	}

	private MimeMessage getMessage() throws AddressException, MessagingException {
		// create some properties and get the default Session
		Properties props = new Properties();
		props.put("mail.smtp.host", "localhost");
		props.put("mail.smtp.port", Configuration.getPort(ConfigConstants.MODE_MAIL));
		props.put("mail.debug", true);

		Session session = Session.getInstance(props, null);
		// session.setDebug(true);
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("sender@mailtest.com"));
		InternetAddress[] address = { new InternetAddress("recipient@mailtest.com") };
		msg.setRecipients(Message.RecipientType.TO, address);
		msg.setSubject("JavaMail APIs Test");
		msg.setSentDate(new Date());

		return msg;

	}

	private void sendMessage(Message message) throws MessagingException {

		Transport.send(message);
	}
}
