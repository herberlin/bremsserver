package de.herberlin.bremsserver.config;

import java.util.prefs.Preferences;

import de.herberlin.server.Logger;

/**
 * New Configuration Service uses the java.util.prefs.Preferences Api;
 *
 * @author hans joachim herbertz created 28.12.2003
 */
public abstract class Configuration {
	/**
	 * Preference name package
	 */
	public static final Class PREF_CLASS = de.herberlin.server.common.AbstractServer.class;
	private static Preferences preferences = Preferences.userNodeForPackage(PREF_CLASS);
	private static Logger logger = Logger.getLogger(PREF_CLASS.getName());

	public static void setPreferencesImpl(Preferences impl) {
		preferences = impl;
	}

	/**
	 * Gets the application root preferences.
	 *
	 * @return
	 */
	public static Preferences getPrefs() {
		return preferences;
	}

	/**
	 * Stores the preferences to file.
	 *
	 * @throws Exception
	 */
	public static void store() {
		try {
			// getPrefs().store();
		} catch (Exception e) {
			logger.warn("Preference storage fails: ", e);
		}
	}

	/**
	 * Gets the mimeType node of the application preferences.
	 *
	 * @return
	 */
	public static Preferences getMimeTypes() {
		String name = "mimeTypes";
		Preferences prefs = getPrefs();
		try {
			if (!prefs.nodeExists(name)) {
				Preferences p = prefs.node(name);
				p.put("jpeg", "image/jpeg");
				p.put("php", "text/html");
				p.put("gif", "image/gif");
				p.put("txt", "text/plain");
				p.put("html", "text/html");
				p.put("htm", "text/html");
				p.put("shtml", "text/html");
				p.put("shtm", "text/html");
				p.put("css", "text/css");
				p.put("htm", "text/html");
				p.put("png", "image/png");
				p.put("jpg", "image/jpeg");
				p.put("jpe", "image/jpeg");
				p.put("zip", "application/zip");
				p.put("js", "text/javascript");

			}
		} catch (Exception e) {
			logger.error("Failed adding default mimetype to preferences: ", e);
		}
		return prefs.node(name);
	}

	/**
	 * Returns the extensions node of the application preferences.
	 *
	 * @return
	 */
	public static Preferences getExtensions() {
		String name = "extension";
		return getPrefs().node(name);

	}

	/**
	 * Returns the Alias Directory node of the application preferences.
	 *
	 * @return
	 */
	public static Preferences getAliasDirectories() {
		String name = "aliasDirectories";
		return getPrefs().node(name);
	}

	public static int getPort(String mode) {
		int result = 0;
		if (ConfigConstants.MODE_HTTP.equals(mode)) {
			result = Configuration.getPrefs().getInt(ConfigConstants.MODE_HTTP + ConfigConstants.SETTING_PORT, 8088);
		} else if (ConfigConstants.MODE_MAIL.equals(mode)) {
			result = Configuration.getPrefs().getInt(ConfigConstants.MODE_MAIL + ConfigConstants.SETTING_PORT, 2525);
		} else if (ConfigConstants.MODE_PROXY.equals(mode)) {
			result = Configuration.getPrefs().getInt(ConfigConstants.MODE_PROXY + ConfigConstants.SETTING_PORT, 2505);
		}
		return result;
	}

	public static void setPort(String mode, int port) {
		getPrefs().putInt(mode + ConfigConstants.SETTING_PORT, port);
		store();
	}

	public static String getDocroot() {
		return getPrefs().get(ConfigConstants.HTTP_DOCROOT, System.getProperty("user.dir"));
	}
	
	public static String getVersion() {
		String version = Configuration.class.getPackage().getImplementationVersion();
		if (version == null) {
			version = "DEV"; 
		}
		return "Bremsserver "+ version; 

	}
	
}
