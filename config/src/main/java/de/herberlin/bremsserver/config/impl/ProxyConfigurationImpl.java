package de.herberlin.bremsserver.config.impl;

import de.herberlin.bremsserver.config.ConfigConstants;
import de.herberlin.bremsserver.config.Configuration;
import de.herberlin.server.proxy.ProxyServerConfig;

public class ProxyConfigurationImpl implements ProxyServerConfig {

	@Override
	public int getDelay() {
		return Configuration.getPrefs().getInt(ConfigConstants.MODE_PROXY + ConfigConstants.SETTING_DELAY, 0);
	}

	@Override
	public boolean isNoCaching() {
		return Configuration.getPrefs().getBoolean(ConfigConstants.PROXY_NO_CACHING_HEADERS, true);
	}

}
