package de.herberlin.bremsserver.config;


/**
 * Holds names of the configuration keys.
 *
 * @author Hans Joachim Herbertz
 * @created 27.01.2003
 */
public interface ConfigConstants {

	/**
	 * The name of the configuration file.
	 */
	String CONFIG_FILE = "bremsserver.xml";
	boolean USE_CONFIG_FILE = true;

	String MODE_HTTP = "http";
	String MODE_PROXY = "proxy";
	String MODE_MAIL = "mail";


	String SETTING_PORT = ".port";
	String SETTING_DELAY = ".delay";


	/**
	 * Number of rows in the displayed server log. */
	String LOGSIZE = "ui.logsize";

	/**
	 * The value of the selected server (Http or proxy)
	 */
	String SERVER_SERVER = "server.server";

	String DISPLAY_TEXTONLY = "display.textonly";

	/**
	 * Boolean value; true if the server shows default pages
	 * for a directory request; else shows directory index
	 */
	String HTTP_USE_DEFAULT_PAGES = "http.userdefaultpages";

	/**
	 * List of defaultpages to show instead of a directory list
	 */
	String HTTP_DEFAULT_PAGE_LIST = "http.defaultpagelist";
	/**
	 * Boolean value; true if the server sends headers to avoid caching
	 */
	String HTTP_NO_CACHING_HEADERS = "server.nocache";
	/**
	 * Boolean value; true if the server sends headers to avoid caching
	 */
	String PROXY_NO_CACHING_HEADERS = "proxy.nocache";

	/**
	 * The document root for the http server.
	 */
	String HTTP_DOCROOT = "http.docroot";

}
