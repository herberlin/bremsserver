package de.herberlin.bremsserver.config.impl;

import java.util.prefs.Preferences;

import de.herberlin.bremsserver.config.ConfigConstants;
import de.herberlin.bremsserver.config.Configuration;
import de.herberlin.server.httpd.HttpServerConfig;

public class HttpConfigurationImpl implements HttpServerConfig {

	@Override
	public Preferences getMimeTypes() {
		return Configuration.getMimeTypes();
	}

	@Override
	public Preferences getExtensions() {
		return Configuration.getExtensions();
	}

	@Override
	public Preferences getAliasDirectories() {
		return Configuration.getAliasDirectories();
	}

	@Override
	public String getDefaultPages() {
		return Configuration.getPrefs().get(ConfigConstants.HTTP_DEFAULT_PAGE_LIST, "index.htm,index.html,index.shtml");
	}

	@Override
	public String getDocRoot() {
		return Configuration.getDocroot();
	}

	@Override
	public String getHost() {
		return Configuration.getPrefs().get("host", "localhost");
	}

	@Override
	public String getVersion() {
		return Configuration.getVersion();
	}

	@Override
	public int getDelay() {
		return Configuration.getPrefs().getInt(ConfigConstants.MODE_HTTP + ConfigConstants.SETTING_DELAY, 0);
	}

	@Override
	public boolean isNoCaching() {
		return Configuration.getPrefs().getBoolean(ConfigConstants.HTTP_NO_CACHING_HEADERS, true);
	}

	@Override
	public boolean useDefaultPages() {
		return Configuration.getPrefs().getBoolean(ConfigConstants.HTTP_USE_DEFAULT_PAGES, true);
	}

	@Override
	public String getJsonEventPath() {
		// TODO make this configurable
		return "/maildata.json";
	}

}
