Mail-Server-Portlet
===================

Liferay Portlet that provides an internal mail - server to Liferay portal for
development and testing purposes. The portlet is currently developed for Liferay 6.2.

Please find downloads [here](https://bitbucket.org/herberlin/bremsserver/downloads/)
Please report issues [here](https://bitbucket.org/herberlin/bremsserver/issues/)

TODOS and Issues
----------------
* Input fields for testmails

Screenshot
----------
![screenshot](./support/Auswahl_003.png)