package de.herberlin.server.httpd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.herberlin.server.Logger;
import de.herberlin.server.common.HttpData;
import de.herberlin.server.httpd.HttpServerConfig.Factory;
import de.herberlin.wwwutil.ResponseHeader;
import de.herberlin.wwwutil.SimpleResponse;
import de.herberlin.wwwutil.httperror.ConnectionLost;
import de.herberlin.wwwutil.httperror.HttpError;
import de.herberlin.wwwutil.httperror.InternalServerError_500;
import de.herberlin.wwwutil.httperror.NotFound_404;

/**
 * Class handling simple Http Request
 * 
 * @author hans joachim herbertz created 28.01.2004
 */
public class BasicHttpHandler {

	/**
	 * All data from the thread;
	 */
	protected HttpData data = null;
	/**
	 * Logger
	 */
	protected Logger logger = Logger.getLogger(getClass().getName());

	/**
	 * The pause as coming from configuration
	 */
	protected int pause = 0;

	/**
	 * Constructor
	 * 
	 * @param data
	 */
	public BasicHttpHandler(HttpData data) {
		this.data = data;
		if (Factory.getConfig() != null) {
			pause = Factory.getConfig().getDelay() * 10;
		}
	}

	
	
	/**
	 * Call this to start the process.
	 * 
	 * @throws HttpError
	 */
	public void perform() throws HttpError {
		try {
			perform(new FileInputStream(data.realPath));
		} catch (HttpError he) {
			throw he;
		} catch (Exception e) {
			NotFound_404 ex = new NotFound_404(e);
			logger.debug("File not found: " + data, e);
			throw ex;
		}
	}
	public void perform(InputStream inputStream) throws HttpError {
		data.resp = new SimpleResponse();
		data.resp.setHeader("Content-Type", data.mimeType);
		moreHeaders(data.resp);

		try {
			data.resp.write(data.out);
			int length = writePaused(inputStream, data.out, pause);
			data.resp.setHeader("Content-Length", length + "");

			// set display data
			data.fileData.setContentType(data.mimeType);
			if (data.realPath != null) {
				data.fileData.setContentLength(data.realPath.length());
				data.fileData.setFile(data.realPath);
			}

		} catch (FileNotFoundException e1) {
			NotFound_404 ex = new NotFound_404(e1);
			logger.debug("File not found: " + data, ex);
			throw ex;
		} catch (Exception e1) {
			InternalServerError_500 ex = new InternalServerError_500(e1);
			logger.debug("IOError " + data, ex);
			throw ex;
		}

	}

	/**
	 * Writes inputstream to the outputsteam using pause in ms.
	 * 
	 * @param in
	 * @param out
	 * @throws IOException
	 */
	protected int writePaused(InputStream in, OutputStream out, long pause) throws IOException {
		int length = 0;
		byte[] buffer = new byte[256];
		int rest = -1;
		while ((rest = in.read(buffer)) != -1) {
			if (pause > 0) {
				try {
					Thread.sleep(pause);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			out.write(buffer, 0, rest);
			out.flush();
			length += rest;
		}
		return length;
	}

	/**
	 * Sets additional headers and the no cache headers to the response if
	 * configuration requires it; if caching is allowed does nothing.
	 */
	protected void moreHeaders(ResponseHeader resp) {

		resp.setHeader("Server", Factory.getConfig().getVersion());
		resp.setHeader("Connection", "close");

		if (Factory.getConfig().isNoCaching()) {
			resp.setHeader("Expires", "Thu, 19 Nov 1981 08:52:00 GMT");
			resp.setHeader("Cache-Control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
			resp.setHeader("Pragma", "no-cache");
		}
	}

}
