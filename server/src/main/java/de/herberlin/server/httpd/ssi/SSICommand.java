package de.herberlin.server.httpd.ssi;

public class SSICommand {

	private int command;
	private int subcommand;
	private String message;
	private String fileType;

	public SSICommand() {
		command = 0;
		subcommand = 0;
		message = "";
		fileType = "";
	}

	public SSICommand(int aCommand, int aSubCommand, String aMessage) {
		command = aCommand;
		subcommand = aSubCommand;
		message = aMessage;
		fileType = "";
	}

	public SSICommand(int aCommand, String aFileType, String aMessage) {
		command = aCommand;
		subcommand = 0;
		message = aMessage;
		fileType = aFileType;
	}

	public int getCommand() {
		return command;
	}

	public void setCommand(int aCommand) {
		command = aCommand;
	}

	public int getSubCommand() {
		return subcommand;
	}

	public void setSubCommand(int aSubCommand) {
		subcommand = aSubCommand;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String aMessage) {
		message = aMessage;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String aFileType) {
		fileType = aFileType;
	}

	public String toString() {
		return new String("Command: " + command + " : " + message);
	}
}