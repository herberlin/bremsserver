package de.herberlin.server.httpd;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import de.herberlin.server.Logger;
import de.herberlin.server.common.HttpData;
import de.herberlin.server.common.event.ApplicationEvent;
import de.herberlin.server.common.event.EventDispatcher;
import de.herberlin.server.httpd.HttpServerConfig.Factory;
import de.herberlin.wwwutil.RequestHeader;
import de.herberlin.wwwutil.httperror.ConnectionLost;
import de.herberlin.wwwutil.httperror.HttpError;
import de.herberlin.wwwutil.httperror.InternalServerError_500;
import de.herberlin.wwwutil.httperror.MovedTemporarily_302;
import de.herberlin.wwwutil.httperror.NotFound_404;

public class HttpThread implements Runnable {

	static Logger logger = Logger.getLogger(HttpThread.class.getName());
	private final Socket client;
	private Thread theThread;

	/**
	 * Constructor
	 */
	public HttpThread(Socket socket) {
		client = socket;
		theThread = new Thread(this);
		theThread.start();
		logger.debug("Thread started for socket=" + socket);
	}

	public static File getMappedFilename(String reqFile, URL referringUrl)
			throws NotFound_404, InternalServerError_500 {
		URL newUrl = null;
		try {
			newUrl = new URL(referringUrl, reqFile);
		} catch (MalformedURLException e) {
			logger.error("getMappedFilename: " + reqFile, e);
			throw new InternalServerError_500(e);
		}
		logger.debug("getMappedFilename url=" + newUrl);
		return getMappedFilename(newUrl.getPath());
	}

	/**
	 * maps url to filesystem
	 */
	public static File getMappedFilename(String reqFile) throws InternalServerError_500, NotFound_404 {
		if (!reqFile.startsWith("/")) {
			reqFile = "/" + reqFile;
		}
		String path = Factory.getConfig().getDocRoot() + reqFile;
		String[] keys = null;

		try {
			keys = Factory.getConfig().getAliasDirectories().keys();
		} catch (Exception e) {
			logger.error("getMappedFilename", e);
			throw new InternalServerError_500(e);
		}

		for (int i = 0; i < keys.length; i++) {
			if (reqFile.indexOf(keys[i]) == 0) {
				path = Factory.getConfig().getAliasDirectories().get(keys[i], null) + reqFile.substring(keys[i].length());
			}
		}

		File f = new File(path.replace('/', File.separatorChar));
		logger.debug("MappedFilename for: " + reqFile + " is: " + f);

		if (f.exists()) {
			return f;
		} else {
			NotFound_404 ex = new NotFound_404("File not Found: " + reqFile);
			logger.error("getMappedFilename", ex);
			throw ex;
		}
	}

	public void run() {

		EventDispatcher.add(new ApplicationEvent(ApplicationEvent.CONNECTION_ESTABLISHED));

		HttpData data = new HttpData();
		data.port = client.getLocalPort();
		data.inetAddress = client.getInetAddress();

		try {

			// setup httpdata
			try {
				data.req = new RequestHeader(client.getInputStream());
				data.out = new BufferedOutputStream(client.getOutputStream());
			} catch (IOException e) {
				ConnectionLost ex = new ConnectionLost(e);
				logger.debug(ex + "");
				throw ex;
			}

			if (data.req.getPath().equals(Factory.getConfig().getJsonEventPath())) {
				
				new JsonEventHandler(data).perform();
				
			} else if ((data.realPath = getMappedFilename(data.req.getPath())).isDirectory()) {

				new DirectoryListHttpHandler(data).perform();

			} else /* Not a directory but a file */ {

				String fileType = data.req.getFileType();
				data.mimeType = Factory.getConfig().getMimeTypes().get(fileType, "application/octet-stream");
				logger.debug("FileType=" + fileType + " MimeType=" + data.mimeType);

				if (fileType.indexOf("shtm") == 0) {

					new SsiHandler(data).perform();

				} else if (Factory.getConfig().getExtensions().get(fileType, null) != null) {

					new CgiRequestHandler(data).perform();

				} else {

					// print ordinary file
					new BasicHttpHandler(data).perform();
				}
			}

			// closing
			try {
				data.out.close();
			} catch (IOException e) {
				logger.debug(e + "");
			}
			logger.debug("Successfull:" + data.req.getPath());
		} catch (ConnectionLost e) {
			// do nothing
		} catch (InternalServerError_500 e) {
			HttpServer.writeHttpError(e, data);
		} catch (MovedTemporarily_302 e) {
			HttpServer.writeHttpError(e, data);
		} catch (NotFound_404 e) {
			HttpServer.writeHttpError(e, data);
		} catch (HttpError e) {
			HttpServer.writeHttpError(e, data);
		} catch (Throwable t) {
			HttpServer.writeHttpError(new InternalServerError_500(t), data);
		} finally {
			try {
				client.close();
			} catch (Throwable t) {
				logger.debug(t + "");
			}
			EventDispatcher.add(data.asEvent());
			EventDispatcher.add(new ApplicationEvent(ApplicationEvent.CONNECTION_CLOSED));
		}
	}

}