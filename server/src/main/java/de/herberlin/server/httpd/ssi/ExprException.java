package de.herberlin.server.httpd.ssi;
/** Exception for de.herberlin.util.Expr
*	@author Hans Joachim Herbertz www.herberlin.de	*/
public class ExprException extends Exception {
	ExprException(String msg) {super("Invalid operation: "+msg);}
}
	
