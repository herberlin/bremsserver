package de.herberlin.server.httpd.ssi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties; 

/**	Class converts a C style DateFormat to 
*	a java DateFormat String for use with 
*	simple DateFormat. You can use the main()
*	method for mapping detals.
*	<br>
*	Please note: The following items are not or 
*	not well supported:
*	<ul><li>%w should be mapped into number of a day in the week
*	(eg. 1 for Monday) but is mapped into 2 char day name (eg. Mo).
*	<li>%W Should be mapped into number of week in year where a week
*	starts on monday but is mapped into number of week in year where
*	week starts on sunday (same as %U).
*	</ul>
*	@author hans joachim herbertz 25.04.2002
*	@see SimpleDateFormat */
public class DateFormatExchanger {
	
	/** Hashtable for character mapping */
	private final Properties data;

	/** Constructor creates global data */
	public DateFormatExchanger() {
		data=makeReplaceTable();
	}

	/** You can run this class to become familiar with
	*	the use of the C style date format string.
	*	For the use of this class within your application
	 *	we recommend to remove it */
	public static void main(String args[]) {

		String[] tests=new String[]{
			"%%a	Abbreviated weekday name: %a",
			"%%A	Full weekday name: %A",
			"%%b	Abbreviated month name: %b",
			"%%B	Full month name: %B",
			"%%c	Date and time: %c",
			"%%d	Two-digit day of month (01 - 31): %d",
			"%%H	Hour of the day, 24 hour day: %H",
			"%%I	Two-digit hour, 12 hour day (01 - 12): %I",
			"%%j	Three-digit day of year (001 - 366): %j",
			"%%m	Two-digit month as a decimal number (1 - 12): %m",
				"%%M	2-digit minute (00 - 59): %M",
			"%%p	AM or PM: %p",
			"%%S	Two-digit second (00 - 59): %S",
			"%%U	Two-digit week number where Sunday is the first day of the week (00 - 53): %U",
			"%%w	Weekday where 0 is Sunday (0 - 6): %w (Bad implementation).",
			"%%W	Two-digit week number where Monday is the first day of the week (00 - 53): %W (Bad implementation).",
			"%%x	Date: %x",
			"%%X	Time: %X",
			"%%y	Two-digit year without century (00 to 99): %y",
			"%%Y	Year with century: %Y",
			"%%z	Time zone name, or no characters if no time zone: %z",
			"%%Z	Time zone name, or no characters if no time zone: %Z"
		};

		DateFormatExchanger me = new DateFormatExchanger();
		for(int i=0;i<tests.length;i++) {
			String format=me.cToJava(tests[i]);
			SimpleDateFormat f=new SimpleDateFormat(format);
			System.out.println (f.format(new Date()));
		}
	}

	/**
	 * Working method. Converts a C style DateFormat String
	 * into a Java stile DateFormat String for use with
	 * java.text.SimpleDateFomrat
	 *
	 * @param String A C date format string
	 * @return String A java style date format String
	 */
	public String cToJava(final String aCDateFormatString) {
		StringBuffer in = new StringBuffer(aCDateFormatString);
		StringBuffer out = new StringBuffer(aCDateFormatString.length());

		char current = ' ';
		char target = '%';
		char sep = '\'';

		for (int i = 0; i < aCDateFormatString.length(); i++) {
			current = in.charAt(i);
			if (current == target) {
				if (i != 0) out.append(sep); // Closing String sequence
				i++;
				String t = (String) data.get(String.valueOf(in.charAt(i)));
				if (t != null) out.append(t);
				if (i != aCDateFormatString.length() - 1) out.append(sep);
			} else {
				if (i == 0) out.append(sep);
				out.append(current);
				if (current == sep) out.append(sep);
				if (i == aCDateFormatString.length() - 1) out.append(sep);
			}
		}
		return out.toString();
	}

	/** Builds the mapping hash */
	private Properties makeReplaceTable(){
		Properties p=new Properties();	
		p.put("a","E");
		p.put("A","EEEE");
		p.put("b","MMM");
		p.put("B","MMMM");
		p.put("c","yyyy-MM-dd, hh:mm:ss");
		p.put("d","dd");
		p.put("H","H");
		p.put("I","h");
		p.put("j","DDD");
		p.put("m","MM");
		p.put("M","mm");
		p.put("p","aa");
		p.put("S","ss");
		p.put("U","ww");
		p.put("w","EE");
		p.put("W","ww");
		p.put("x","yyyy-MM-dd");
		p.put("X","hh:mm:ss");
		p.put("y","yy");
		p.put("Y","yyyy");
		p.put("z","z");
		p.put("Z","zzzz");
		p.put("%","%");
		return p;
	}
}
