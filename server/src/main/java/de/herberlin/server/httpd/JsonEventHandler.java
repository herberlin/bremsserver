package de.herberlin.server.httpd;

import java.io.ByteArrayInputStream;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import de.herberlin.server.common.HttpData;
import de.herberlin.server.common.event.EventDispatcher;
import de.herberlin.server.common.event.MailRequestEvent;
import de.herberlin.server.common.event.ServerEvent;
import de.herberlin.server.common.event.ServerEventListener;
import de.herberlin.wwwutil.httperror.HttpError;

public class JsonEventHandler extends BasicHttpHandler {

	public static class MailEventStore implements ServerEventListener {
		
		private static MailEventStore _instance = null; 
		public static MailEventStore getInstance() {
			if (_instance == null ) {
				_instance = new MailEventStore();
				EventDispatcher.addServerEventListener(_instance);
			}
			return _instance;
		}
		
		private List<MailRequestEvent> eventList = new LinkedList<>();
		private  int maxSize = 10;


		public static List<MailRequestEvent> getEventList() {
			return new LinkedList<MailRequestEvent>(getInstance().eventList);
		}
		@Override
		public void onServerEvent(ServerEvent ev) {
			if (ev instanceof MailRequestEvent) {
				eventList.add(0, (MailRequestEvent) ev);

				if (eventList.size() > maxSize) {
					eventList.remove(maxSize);
				}
			}
		};
	}

	public JsonEventHandler(HttpData data) {
		super(data);

	}


	@Override
	public void perform() throws HttpError {
		data.mimeType = "text/json";

		perform(new ByteArrayInputStream(getJson().getBytes()));
	}

	private String getJson() {
		JSONArray arr = new JSONArray();
		for (MailRequestEvent ev : MailEventStore.getEventList()) {
			JSONObject obj = new JSONObject();
			obj.put("time", ev.getValueAt(0));
			obj.put("from", ev.getValueAt(1));
			obj.put("to", ev.getValueAt(2));
			JSONObject headers = new JSONObject();
			obj.put("headers", headers);
			for (String[] kv : ev.getHeaders().getHeader()) {
				headers.put(kv[0], kv[1]);
			}

			obj.put("content", ev.getHeaders().getContent());
			obj.put("raw", ev.getConversation());
			logger.debug("Writing json:" + obj);
			arr.put(obj);
		}
		return arr.toString();
	}
}
