package de.herberlin.server.httpd;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.net.Socket;

import de.herberlin.server.common.AbstractServer;
import de.herberlin.server.common.HttpData;
import de.herberlin.server.httpd.JsonEventHandler.MailEventStore;
import de.herberlin.wwwutil.ProxyResponse;
import de.herberlin.wwwutil.httperror.HttpError;

/**
 * Http Server to set up the old Bremsserver Module
 *
 * @author hans joachim herbertz created 27.12.2003
 */
public class HttpServer extends AbstractServer {

	public HttpServer(HttpServerConfig config) {
		if (config != null){
			HttpServerConfig.Factory.setConfig(config);
		}

	}
	
	public HttpServer() {
		
	}


    @Override
	public void start(int port) {
		super.start(port);
		if (HttpServerConfig.Factory.getConfig().getJsonEventPath() != null) {
			MailEventStore.getInstance(); // start the mail event listener
		}
	}

	protected void process(Socket client) {
		new HttpThread(client);
	}


	public static void writeHttpError(HttpError error, HttpData data) {
		if (data.out != null) {
	
			try {
				error.write(data.out);
				data.out.flush();
	
				// event FileData
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				error.write(out);
				byte[] errorHtml = out.toByteArray();
				data.fileData.setContentLength(errorHtml.length);
				data.fileData.setContentType("text/html");
				data.fileData.setData(errorHtml);
	
				// Event response
				data.resp = new ProxyResponse(new ByteArrayInputStream(errorHtml));
	
			} catch (Exception e) {
				HttpThread.logger.error("Error writing error data", e);
			}
		} else {
			HttpThread.logger.warn("OutputStream in null.");
		}
	}

}
