package de.herberlin.server.proxy;

import java.util.prefs.Preferences;

public interface ProxyServerConfig {


	int getDelay();
	boolean isNoCaching();
	
	public static class Factory{
		
		private static ProxyServerConfig config = null; 
		
		public static ProxyServerConfig getConfig() {
			return config; 
		}
		public static void setConfig(ProxyServerConfig config){
			Factory.config = config; 
		}
	}
	
	
}
