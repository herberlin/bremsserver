package de.herberlin.server.proxy;

import java.net.Socket;

import de.herberlin.server.common.AbstractServer;

/**
 * @author hans joachim herbertz created 26.12.2003
 */
public class ProxyServer extends AbstractServer {

	public ProxyServer(ProxyServerConfig config) {
		if (config == null) {
			throw new RuntimeException("Config must not be null!");
		}
		ProxyServerConfig.Factory.setConfig(config);

	}

	/**
	 * @see de.herberlin.server.AbstractServer#process(Socket)
	 */
	protected void process(Socket client) {
		new ProxyThread(client);

	}

}
