package de.herberlin.server.mail;

import org.junit.Test;

public class MailUtilTest {
	
	String t = "<p>Die <b>Hypertext Markup Language</b> (<b>HTML</b>, <a href=3D\"/wiki/Engl=\n" + 
			"ische_Sprache\" title=3D\"Englische Sprache\">englisch</a> f=FCr <i><a href=3D=\n" + 
			"\"/wiki/Hypertext\" title=3D\"Hypertext\">Hypertext</a>-Auszeichnungssprache</i=\n" + 
			">) ist eine textbasierte <a href=3D\"/wiki/Auszeichnungssprache\" title=3D\"Au=\n" + 
			"szeichnungssprache\">Auszeichnungssprache</a> zur Strukturierung <a href=3D\"=\n" + 
			"/wiki/Elektronisches_Dokument\" title=3D\"Elektronisches Dokument\">elektronis=\n" + 
			"cher Dokumente</a> wie <a href=3D\"/wiki/Textdatei\" title=3D\"Textdatei\">Text=\n" + 
			"e</a> mit <a href=3D\"/wiki/Hyperlink\" title=3D\"Hyperlink\">Hyperlinks</a>, <=\n" + 
			"a href=3D\"/wiki/Bilddatei\" title=3D\"Bilddatei\">Bildern</a> und anderen Inha= \n" + 
			"lten. <a href=3D\"/wiki/Webseite\" title=3D\"Webseite\">HTML-Dokumente</a> sind=\n" + 
			" die Grundlage des <a href=3D\"/wiki/World_Wide_Web\" title=3D\"World Wide Web=\n" + 
			"\">World Wide Web</a> und werden von <a href=3D\"/wiki/Webbrowser\" title=3D\"W=\n" + 
			"ebbrowser\">Webbrowsern</a> dargestellt. Neben den vom Browser angezeigten I=\n" + 
			"nhalten k=F6nnen HTML-Dateien zus=E4tzliche Angaben in Form von <a href=3D\"=\n" + 
			"/wiki/Metadaten\" title=3D\"Metadaten\">Metainformationen</a> enthalten, z.&nb=\n" + 
			"sp;B. =FCber die im Text verwendeten <a href=3D\"/wiki/Einzelsprache\" title=\n" + 
			"=3D\"Einzelsprache\">Sprachen</a>, den <a href=3D\"/wiki/Autor\" title=3D\"Autor=\n" + 
			"\">Autor</a> oder den zusammengefassten Inhalt des Textes.\n" + 
			"</p>\n" + 
			"";

	@Test
	public void testDecodeQuotedPrintable() {
		
		String[] tt = t.split("\n");
		for (String s : tt) {
			System.out.println(s);
			String r = MailUtil.decodeQuotedPrintableLine(s);
			System.out.println(r);
			System.out.println("");
		}
		
	}
	
	String base64 ="RGF0dW0sIkVpbnMiLCJad2VpIiwiSEhIIgoiMjAyMC0wNC0xNCAxMjo1NDoxNyIsImVpbnMiLCJG\n" + 
			"csO8aGxpbmciLCJleHBhbmRvVGFibGUiCiIyMDIwLTA0LTE0IDEzOjIwOjU5IiwiVmllciIsIiBX\n" + 
			"aW50ZXIiLCJXZWJGb3JtUG9ydGxldC5EQVRFX0NPTFVNTl9OQU1FIgoiMjAyMC0wNC0xNCAxMzoy\n" + 
			"OTo0OCIsIlZpZXIiLCJGcsO8aGxpbmciLCJXZWJGb3JtUG9ydGxldC5EQVRFX0NPTFVNTl9OQU1F\n" + 
			"IgoiMjAyMC0wNC0xNCAxMzo1NjoyOCIsIiIsIkZyw7xobGluZyIsIiIK\n" + 
			"\n";
	@Test
	public void testDecodeBase64() {
		
		String[] tt = base64.split("\n");
		for (String s : tt) {
			System.out.println(s);
			String r = MailUtil.decodeBase64Line(s);
			System.out.println(r);
			System.out.println("");
		}
		
	}
}
