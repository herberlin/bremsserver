Bremsserver Server
==================

Project provides implementations for 
- http - server
- mail server (no relay implementation, just reciever)
- proxy server.

Servers are not for production use but only for testing. Therefore the 
can be configured to set no caching headers and slow down the response.

