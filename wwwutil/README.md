Bremsserver Wwwutil
===================

Package contains utility classes for parsing HTTP Headers and
handling HTTP protocol with different encodings. Please note that
this is not a complete framework like the apache commons but just a 
collections of utility classes used with my Bremsserver
software and other projects.

## Version
###1.1<
Multipart-formdata request is now handled like each other post request.<br>

###1.2
New project structure.