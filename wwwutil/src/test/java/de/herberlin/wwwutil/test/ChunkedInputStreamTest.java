package de.herberlin.wwwutil.test;

import junit.framework.TestCase;

import java.io.InputStream;

import de.herberlin.wwwutil.ChunkedInputStream;


/** Test needs two files:
 * 	chunked.txt for the chunked-version
 *  plain.txt for the plain version
 *
 * @author Hans Joachim Herbertz
 * @created 07.12.2002
 */
public class ChunkedInputStreamTest extends TestCase {

	// public static final String PACKAGE="/de/herberlin/wwwutil/test";
	public static final String PACKAGE="";
	/**
	 * Constructor for ChunkedInputStreamTest.
	 * @param name
	 */
	public ChunkedInputStreamTest(String name) {
		super(name);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(ChunkedInputStreamTest.class);
	}

	/**
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/** 
	 * Test for int read()
	 * Reads the plain.txt and compares it to output of 
	 * ChunkedInputStream
	 */
	public void testRead() throws Exception {
		InputStream in=getClass().getResourceAsStream(PACKAGE+ "/chunked.txt");
		InputStream testIn=getClass().getResourceAsStream(PACKAGE+ "/plain.txt");
		ChunkedInputStream testObj=new ChunkedInputStream(in);
		int b;
		while ((b=testObj.read())!=-1) {
			int shouldBe=testIn.read();
			// System.out.println ((char)b +" - "+ (char) shouldBe);
			assertEquals((char)b,(char)shouldBe);
		}
		testObj.close();
		testIn.close();
	}
	
	/** 
	 * Test for int read()
	 * Reads the plain.txt and compares it to output of 
	 * ChunkedInputStream
	 */
	public void testReadByteArray() throws Exception {
		InputStream in=getClass().getResourceAsStream(PACKAGE+ "/chunked.txt");
		if (in == null) {
			fail("Resource not found at: " + PACKAGE);
		}
		InputStream testIn=getClass().getResourceAsStream(PACKAGE+ "/plain.txt");
		if (testIn == null) {
			fail("Resource not found at: " + PACKAGE);
		}

		ChunkedInputStream testObj=new ChunkedInputStream(in);
		byte[] b=new byte[12];
		byte[] shouldBe=new byte[b.length];
		int tLength=-1;
		int sLength=-1;
		while ((sLength=testObj.read(b))!=-1) {
			// System.out.print (new String(b));
			//System.out.print (new String(shouldBe));
			tLength=testIn.read(shouldBe);
			assertEquals(new String(b),new String(shouldBe));
			assertEquals(sLength,tLength);
		}
		testObj.close();
		testIn.close();
	}
	
	

}
