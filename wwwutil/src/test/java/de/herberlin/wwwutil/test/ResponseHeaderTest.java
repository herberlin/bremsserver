package de.herberlin.wwwutil.test;

import junit.framework.TestCase;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import de.herberlin.wwwutil.CgiResponse;
import de.herberlin.wwwutil.ProxyResponse;
import de.herberlin.wwwutil.ResponseHeader;
import de.herberlin.wwwutil.SimpleResponse;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 01.02.2003
 */
public class ResponseHeaderTest extends TestCase {

	/**
	 * Constructor for ResponseHeaderTest.
	 * @param name
	 */
	public ResponseHeaderTest(String name) {
		super(name);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(ResponseHeaderTest.class);
	}
	private InputStream  getInputStream(String headers) throws Exception {
		BufferedInputStream in=new BufferedInputStream(new ByteArrayInputStream(headers.getBytes()));
		return in;
	}
	public void test200() throws Exception {
		StringBuffer stb=new StringBuffer();
		stb.append("HTTP/1.1 200 OK\r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Server: Apache/2.0.40 (Win32)\r\n");
		stb.append("X-Powered-By: PHP/4.2.3\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Keep-Alive: timeout=15, max=100\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("Content-Type: text/html; charset=ISO-8859-1\r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");
		
		ResponseHeader testObj=new ProxyResponse(getInputStream(stb.toString()));
		assertEquals("HTTP/1.1",testObj.getProtocol());		
		assertEquals(new Integer(200),testObj.getStatus());		
		assertEquals("OK",testObj.getStatusMessage());		

		assertEquals("3062",testObj.getHeader("Content-Length"));	
		assertEquals("Keep-Alive",testObj.getHeader("Connection"));	
		assertNull(testObj.getHeader("NOP"));
		
		testObj.write(System.out);
		ByteArrayOutputStream out=new ByteArrayOutputStream(stb.length());	
		testObj.write(out);
		String[] headers=new String(out.toByteArray()).split("\r\n");
		String target=new String(stb);
		for (int i=0;i<headers.length;i++) {
			assertTrue(target.indexOf(headers[i])>-1);	
		}
		String[] targetSplit=target.split("\r\n");
		assertEquals(headers.length,targetSplit.length-2);
		
				
	}

	public void test304() throws Exception {
		StringBuffer stb=new StringBuffer();
		stb.append("HTTP/1.1 304 Not Modified\r\n");
		stb.append("Date: Sat, 07 Dec 2002 17:51:03 GMT\r\n");
		stb.append("Keep-Alive: timeout: 15, max=100\r\n");
		stb.append("Server: Apache/2.0.40 (Win32)\r\n");
		stb.append("ETag: \"1392d680-a52-43ef8a00\"\r\n");
		stb.append("Connection: Keep-Alive\r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		ResponseHeader testObj=new ProxyResponse(getInputStream(stb.toString()));
		assertEquals("HTTP/1.1",testObj.getProtocol());		
		assertEquals(new Integer(304),testObj.getStatus());		
		assertEquals("Not Modified",testObj.getStatusMessage());		

		assertEquals("\"1392d680-a52-43ef8a00\"",testObj.getHeader("ETag"));	
		assertEquals("Keep-Alive",testObj.getHeader("Connection"));	
		assertNull(testObj.getHeader("NOP"));
		
		testObj.write(System.out);
		ByteArrayOutputStream out=new ByteArrayOutputStream(stb.length());	
		testObj.write(out);
		String[] headers=new String(out.toByteArray()).split("\r\n");
		String target=new String(stb);
		for (int i=0;i<headers.length;i++) {
			assertTrue(headers[i],target.indexOf(headers[i])>-1);	
		}
		String[] targetSplit=target.split("\r\n");
		assertEquals(headers.length,targetSplit.length-2);
	}

	public void testCgiResponse() throws Exception {
		StringBuffer stb=new StringBuffer();
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Server: Apache/2.0.40 (Win32)\r\n");
		stb.append("X-Powered-By: PHP/4.2.3\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Keep-Alive: timeout=15, max=100\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("Content-Type: text/html; charset=ISO-8859-1\r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");
		
		ResponseHeader testObj=new CgiResponse(getInputStream(stb.toString()));
		assertEquals("HTTP/1.1",testObj.getProtocol());		
		assertEquals(new Integer(200),testObj.getStatus());		
		assertEquals("OK",testObj.getStatusMessage());		

		assertEquals("3062",testObj.getHeader("Content-Length"));	
		assertEquals("Keep-Alive",testObj.getHeader("Connection"));	
		assertNull(testObj.getHeader("NOP"));
		
		testObj.write(System.out);
		
	}
	
	public void testSimpleResponse() throws Exception {
		ResponseHeader testObj=new SimpleResponse();
		testObj.setHeader("Connection","close");
		assertEquals("HTTP/1.1",testObj.getProtocol());		
		assertEquals(new Integer(200),testObj.getStatus());		
		assertEquals("OK",testObj.getStatusMessage());	
		assertEquals("close",testObj.getHeader("connection"));	
		
		testObj.write(System.out);	
	}
    
    /**
     * Cgi sets location - header but does not set status header.
     * @throws Exception
     */
    public void testLocationHeader() throws Exception {
        
        StringBuffer stb=new StringBuffer();
        stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
        stb.append("Server: Apache/2.0.40 (Win32)\r\n");
        stb.append("X-Powered-By: PHP/4.2.3\r\n");
        stb.append("Location: http://localhost/nextPage.php\r\n");
        stb.append("Content-Length: 3062\r\n");
        stb.append("Keep-Alive: timeout=15, max=100\r\n");
        stb.append("Connection: Keep-Alive \r\n");
        stb.append("\r\n");
        stb.append("Moved Temporarily\r\n");
        
        ResponseHeader testObj=new CgiResponse(getInputStream(stb.toString()));
        assertEquals("HTTP/1.1",testObj.getProtocol());     
        assertEquals(new Integer(302),testObj.getStatus()); 
            
        assertEquals("Moved Temporarily",testObj.getStatusMessage());  
        
        System.out.println(testObj.getFirstLine());    
        
    }
}
