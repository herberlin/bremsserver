package de.herberlin.wwwutil.test;

import junit.framework.TestCase;

import java.io.ByteArrayOutputStream;

import de.herberlin.wwwutil.httperror.HttpError;
import de.herberlin.wwwutil.httperror.InternalServerError_500;
import de.herberlin.wwwutil.httperror.MovedTemporarily_302;

/**
 * @author Administrator
 */
public class InternalServerError_500Test extends TestCase {

	/**
	 * Constructor for InternalServerError_500Test.
	 * @param arg0
	 */
	public InternalServerError_500Test(String arg0) {
		super(arg0);
	}
	/**
	 * Test for void HttpError()
	 */
	public void testHttpError() throws Exception {
		InternalServerError_500 testObj=new InternalServerError_500();
		assertTextPresent(testObj,"HTTP/1.0 500");
		assertTextPresent(testObj,"<html><body><hr><strong>500 Internal Server Error</strong><hr>");
		// testObj.write(System.out);
	}

	/**
	 * Test for void HttpError(String)
	 */
	public void testHttpErrorString() throws Exception  {
		InternalServerError_500 testObj=new InternalServerError_500("This it the message...");
		assertTextPresent(testObj,"This it the message...");
	}

	/**
	 * Test for void HttpError(String, Throwable)
	 */
	public void testHttpErrorStringThrowable() throws Exception  {
		InternalServerError_500 testObj=new InternalServerError_500("This it the message...",new ArithmeticException("I'm nested.."));
		assertTextPresent(testObj,"This it the message...");
		assertTextPresent(testObj,"I'm nested..");
		assertTextPresent(testObj,"java.lang.ArithmeticException");
		assertTextPresent(testObj,getClass().getName());
	}

	/*
	 * Test for void HttpError(Throwable)
	 */
	public void testHttpErrorThrowable() throws Exception  {
		InternalServerError_500 testObj=new InternalServerError_500(new ArithmeticException("I am the nested message.."));
		assertTextPresent(testObj,"I am the nested message..");
		assertTextPresent(testObj,"java.lang.ArithmeticException");
		assertTextPresent(testObj,getClass().getName());
	}

	private void assertTextPresent(HttpError testObj,String target) throws Exception {
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		testObj.write(out);
		String source=new String(out.toByteArray());
		if (source.indexOf(target)==-1){
			fail("Text: "+ target + " not found in: "+source);
		}
	}
	/**
	 * Must be only one CR between Status and first header
	 * @throws Exception
	 */
	public void testMovedTemporarily() throws Exception {
		MovedTemporarily_302 testObj=new MovedTemporarily_302("/gohome");
		assertTextPresent(testObj,"HTTP/1.0 302 Moved Temporarily");
		assertTextPresent(testObj,"Location: /gohome");
		assertTextPresent(testObj,"Temporarily<br>Location");
		assertTextPresent(testObj,"<a href=\"/gohome\"");
	}
}
