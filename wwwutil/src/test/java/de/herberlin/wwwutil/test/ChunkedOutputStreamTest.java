package de.herberlin.wwwutil.test;

import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import de.herberlin.wwwutil.ChunkedInputStream;
import de.herberlin.wwwutil.ChunkedOutputStream;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 01.02.2003
 */
public class ChunkedOutputStreamTest extends TestCase {

	/**
	 * Constructor for ChunkedOutputStreamTest.
	 * @param name
	 */
	public ChunkedOutputStreamTest(String name) {
		super(name);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(ChunkedOutputStreamTest.class);
	}
	/**
	 * This is a Test with Chunked in/outputstream. */
	public void testAllezRetour() throws Exception {

		int[] buffersizes = new int[] { 31, 64, 512, 1024, 2048, 4096 };
		for (int i = 0; i < buffersizes.length; i++) {
			InputStream in = getClass().getResourceAsStream("/plain.txt");
			ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
			ChunkedOutputStream testObj = new ChunkedOutputStream(outBuffer);
			testObj.setBufferSize(buffersizes[i]);
			int b;
			while ((b = in.read()) != -1) {
				testObj.write(b);
			}
			in.close();
			testObj.close();
			// System.out.println("---\n"+buffersizes[i]+"\n"+new String(outBuffer.toByteArray()));
			ChunkedInputStream chIn =
				new ChunkedInputStream(
					new ByteArrayInputStream(outBuffer.toByteArray()));
			InputStream compareIn =  getClass().getResourceAsStream("/plain.txt");
			while ((b = chIn.read()) != -1) {
				//System.out.write(b);
				// System.out.flush();
				int comp = compareIn.read();
				// System.out.write(comp);
				assertEquals(comp, b);
			}
			chIn.close();
			compareIn.close();
		}
	}
}
