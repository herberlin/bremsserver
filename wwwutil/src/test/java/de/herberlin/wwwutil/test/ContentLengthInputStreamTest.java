package de.herberlin.wwwutil.test;

import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.util.Random;

import de.herberlin.wwwutil.ContentLengthInputStream;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 07.12.2002
 */
public class ContentLengthInputStreamTest extends TestCase {

	private byte[] bytes=null;
	private ByteArrayInputStream in=null;
	private Random rand=new Random(System.currentTimeMillis());
	private int readMe=0;
	private ContentLengthInputStream testObj=null;
	
	/**
	 * Constructor for ContentLengthInputStreamTest.
	 * @param name
	 */
	public ContentLengthInputStreamTest(String name) {
		super(name);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(ContentLengthInputStreamTest.class);
	}

	/**
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		bytes =new byte[rand.nextInt(32000)];
		rand.nextBytes(bytes);
		in=new ByteArrayInputStream(bytes);
		readMe=rand.nextInt(bytes.length);
		testObj=new ContentLengthInputStream(in,(long)readMe);
	}

	/*
	 * Test for int read()
	 */
	public void testRead() throws Exception {
		
		for (int i=0;i<readMe;i++) {
			assertTrue("Read",-1!=testObj.read()) ;
		}		
		assertEquals("EOF",-1,testObj.read());
	}

	/*
	 * Test for int read(byte[])
	 */
	public void testReadBArray() throws Exception {
		byte[] step=new byte[rand.nextInt(readMe)];
		//System.out.println("ReadMe="+readMe);
		//System.out.println("StreamLength="+bytes.length);
		//System.out.println("StepLength="+step.length);
		int current =0;
		while (current<readMe-step.length) {
			assertEquals("Full step failed:",step.length,testObj.read(step));	
			current+=step.length;
			//System.out.println("Current: "+current);
		}
		assertEquals("Last step failed:",readMe-current,testObj.read(step));
		assertEquals("Exhausted failed:",-1,testObj.read(step));
	}

	public void testSkip() throws Exception {
		assertEquals("Skip length:",readMe-1,(int)testObj.skip((long)(readMe-1)));
		assertTrue("Read failed",-1!=testObj.read());
		assertTrue("EOF failed",-1==testObj.read());
	}

	public void testAvailable() throws Exception {
		assertEquals("All available",readMe,testObj.available());
		for (int i=0;i<readMe;i++) {
			assertTrue("Read fails:",-1!=testObj.read());
			assertEquals("Available",readMe-i-1,testObj.available());	
		}
		assertEquals("Null available",0,testObj.available());
		assertEquals("EOF",-1,testObj.read());
	}

}
