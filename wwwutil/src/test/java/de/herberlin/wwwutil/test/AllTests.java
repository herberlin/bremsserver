package de.herberlin.wwwutil.test;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 27.01.2003
 */
public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for de.herberlin.wwwutil");
		//$JUnit-BEGIN$
		suite.addTest(new TestSuite(ChunkedInputStreamTest.class));
		suite.addTest(new TestSuite(ChunkedOutputStreamTest.class));
		suite.addTest(new TestSuite(ContentLengthInputStreamTest.class));
		suite.addTest(new TestSuite(InternalServerError_500Test.class));
		suite.addTest(new TestSuite(RequestHeaderTest.class));
		suite.addTest(new TestSuite(ResponseHeaderTest.class));
		//$JUnit-END$
		return suite;
	}
}
