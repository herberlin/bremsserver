package de.herberlin.wwwutil.test;

import junit.framework.TestCase;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import de.herberlin.wwwutil.RequestHeader;

/**
 *
 * @author Hans Joachim Herbertz
 * @created 30.01.2003
 */
public class RequestHeaderTest extends TestCase {

	/**
	 * Constructor for RequestHeaderTest.
	 * @param name
	 */
	public RequestHeaderTest(String name) {
		super(name);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(RequestHeaderTest.class);
	}
	private RequestHeader getRequestHeader(String headers) throws Exception {
		BufferedInputStream in =
			new BufferedInputStream(
				new ByteArrayInputStream(headers.getBytes()));
		RequestHeader testObj = new RequestHeader(in);
		in.close();
		return testObj;
	}
	public void testGetHttp10() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET /path/to/file.html HTTP/1.1 \r\n");
		stb.append("Host: herberlin.de\r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.1", testObj.getProtocol());
		assertEquals("herberlin.de", testObj.getHost());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.html", testObj.getPath());
		/*
				assertEquals("3062",testObj.getHeader("Content-Length"));	
				assertEquals("Keep-Alive",testObj.getHeader("Connection"));	
				assertNull(testObj.getHeader("NOP"));
				
				testObj.write(System.out);
				ByteArrayOutputStream out=new ByteArrayOutputStream(stb.length());	
				testObj.write(out);
				String[] headers=new String(out.toByteArray()).split("\r\n");
				String target=new String(stb);
				for (int i=0;i<headers.length;i++) {
					assertTrue(target.indexOf(headers[i])>-1);	
				}
				String[] targetSplit=target.split("\r\n");
				assertEquals(headers.length,targetSplit.length-2);
		*/

	}
	public void testGetHttp11() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET http://herberlin.de/path/to/file.html HTTP/1.1 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.1", testObj.getProtocol());
		assertEquals("herberlin.de", testObj.getHost());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.html", testObj.getPath());
	}
	public void testGetHttp10NoHost() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET /path/to/file.html HTTP/1.0 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.0", testObj.getProtocol());
		assertNull(testObj.getHost());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.html", testObj.getPath());
	}
	public void testGetHeaders() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET /path/to/file.html HTTP/1.1 \r\n");
		stb.append("Host: herberlin.de\r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.1", testObj.getProtocol());
		assertEquals("herberlin.de", testObj.getHost());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.html", testObj.getPath());

		assertEquals("3062", testObj.getHeader("Content-Length"));
		assertEquals("Keep-Alive", testObj.getHeader("Connection"));
		assertNull(testObj.getHeaderItem("NOP"));
		/*		
				testObj.write(System.out);
				ByteArrayOutputStream out=new ByteArrayOutputStream(stb.length());	
				testObj.write(out);
				String[] headers=new String(out.toByteArray()).split("\r\n");
				String target=new String(stb);
				for (int i=0;i<headers.length;i++) {
					assertTrue(target.indexOf(headers[i])>-1);	
				}
				String[] targetSplit=target.split("\r\n");
				assertEquals(headers.length,targetSplit.length-2);
		*/
	}
	
	public void testAddRemoveHeaders() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET /path/to/file.html HTTP/1.1 \r\n");
		stb.append("Host: herberlin.de\r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("herberlin.de",testObj.getHeader("hOsT"));	
		testObj.setHeader("host","nein.de");
		assertEquals("nein.de",testObj.getHeader("HoSt"));
		assertEquals("nein.de",testObj.getHost());
		testObj.removeHeader("HOST");
		assertNull(testObj.getHeader("Host"));	
		assertNull(testObj.getHeaderItem("hosT"));
	}
	
	public void testPort_portGiven() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET http://herberlin.de:8080/path/to/file.html HTTP/1.1 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.1", testObj.getProtocol());
		assertEquals("herberlin.de", testObj.getHost());
		assertEquals(new Integer(8080),testObj.getPort());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.html", testObj.getPath());
	}
	public void testPort_portNotGiven() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET /path/to/file.html HTTP/1.0 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.0", testObj.getProtocol());
		assertNull(testObj.getHost());
		assertNull(testObj.getPort());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.html", testObj.getPath());
	}
	public void testGetUrlParameters() throws Exception {
		StringBuffer stb = new StringBuffer();
		stb.append("GET http://herberlin.de/path/to/file.php#blau?oma=lieb&opa=dumm HTTP/1.1 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append("NOP: dies nicht\r\n");

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.1", testObj.getProtocol());
		assertEquals("herberlin.de", testObj.getHost());
		assertEquals("GET", testObj.getMethod());
		assertEquals("/path/to/file.php", testObj.getPath());
	}
	public void testPostData() throws Exception {
		String testString="eins=text+eins&zwei=text+zwe%F6";
		StringBuffer stb = new StringBuffer();
		stb.append("POST http://herberlin.de/path/to/file.php HTTP/1.1 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 31\r\n");
		stb.append("Content-Type: application/x-www-form-urlencoded");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		stb.append(testString);

		RequestHeader testObj = getRequestHeader(stb.toString());
		assertEquals("HTTP/1.1", testObj.getProtocol());
		assertEquals("herberlin.de", testObj.getHost());
		assertEquals("POST", testObj.getMethod());
		assertEquals("/path/to/file.php", testObj.getPath());
		assertEquals(testString,new String(testObj.getPostData()));
	}
	
	public void testBugWriteQuerystringHttp11() throws Exception {

		StringBuffer stb = new StringBuffer();
		stb.append("GET http://herberlin.de/path/to/file.php#blau?oma=lieb&opa=dumm HTTP/1.1 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		
		RequestHeader testObj=getRequestHeader(stb.toString());
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		testObj.write(out);
		String result=out.toString();
		System.out.println(result);
		assertTrue(result.indexOf("path/to/file.php#blau?oma=lieb&opa=dumm")>0);
		
	}
	public void testBugWriteQuerystringHttp10() throws Exception {

		StringBuffer stb = new StringBuffer();
		stb.append("GET http://herberlin.de/path/to/file.php#blau?oma=lieb&opa=dumm HTTP/1.0 \r\n");
		stb.append("Date: Fri, 06 Dec 2002 20:37:11 GMT\r\n");
		stb.append("Content-Length: 3062\r\n");
		stb.append("Connection: Keep-Alive \r\n");
		stb.append("\r\n");
		
		RequestHeader testObj=getRequestHeader(stb.toString());
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		testObj.write(out);
		String result=out.toString();
		System.out.println(result);
		assertTrue(result.indexOf("path/to/file.php#blau?oma=lieb&opa=dumm")>0);
		
	}
}
