5b7
<h3>Expression</h3>
<p>
    This is a simple recursive parser for Expressions.
    It tries to follow the standards of gnu expr, but does not understand 
    regular expressions. I found it usefull to have a lightwight replacement
    for the gnu.expr.Parser to use with java code. 
</p>
<p>
    The code is <a href="http://www.gnu.org" target="_blank">free software</a>. 
    Its easy use from java code: Just call: <code>public static synchronized boolean
    de.herberlin.util.Expr.parse(java.lang.String) throws de.herberlin.util.ExprException</code>.
    The parser returns true if the given expression results to true, else false, and 
    throws exception if it can't be parsed.
</p>
<p>
    A main() - method is implemented so you can use the parser at the command line or
    within stream, althought you might find better things for that. Example: <code>
    echo 3*3==9 | java -jar Expr.jar</code> returns <code>true</code>.
</p>
<p>
    This is a little applet that shows how the parser works:
</p><br>

<table><tr><td valign="top" height="300">
    <applet code="de.herberlin.gui.ExprApplet" codebase="tools/media/" archive="expr_applet.jar" width="455" height="300">
    </applet>
</td></tr></table>
<ul>
    <li><a href="tools/expression_source.html" target="_blank">See source.</a></li>
    <li><a href="tools/download/index.php?download=Expr.zip">Download *.zip 13kb (Source included).</a></li>
</ul><br>
<br>



5b7
<h3>Expression</h3>
<p>
    This is a simple recursive parser for Expressions.
    It tries to follow the standards of gnu expr, but does not understand 
    regular expressions. I found it usefull to have a lightwight replacement
    for the gnu.expr.Parser to use with java code. 
</p>
<p>
    The code is <a href="http://www.gnu.org" target="_blank">free software</a>. 
    Its easy use from java code: Just call: <code>public static synchronized boolean
    de.herberlin.util.Expr.parse(java.lang.String) throws de.herberlin.util.ExprException</code>.
    The parser returns true if the given expression results to true, else false, and 
    throws exception if it can't be parsed.
</p>
<p>
    A main() - method is implemented so you can use the parser at the command line or
    within stream, althought you might find better things for that. Example: <code>
    echo 3*3==9 | java -jar Expr.jar</code> returns <code>true</code>.
</p>
<p>
    This is a little applet that shows how the parser works:
</p><br>

<table><tr><td valign="top" height="300">
    <applet code="de.herberlin.gui.ExprApplet" codebase="tools/media/" archive="expr_applet.jar" width="455" height="300">
    </applet>
</td></tr></table>
<ul>
    <li><a href="tools/expression_source.html" target="_blank">See source.</a></li>
    <li><a href="tools/download/index.php?download=Expr.zip">Download *.zip 13kb (Source included).</a></li>
</ul><br>
<br>



0
