import { LitElement, html, css } from 'lit-element';


// Extend the LitElement base class
class MailServer extends LitElement {

  static get styles() {
    return css`
    .hide {
      display: none
    }

    .container {
      max-width: 100%
    }

    .panel-body {
      padding: 0 15px 30px 15px;
    }
    .panel-body pre{
      height: 400px; 
    }
    .panel-body .tab-content pre code{
      white-space: pre-line; 
    }
    .panel-body .tab-raw pre code{
      white-space: pre; 
    }
    `
  }
  
  static get properties() {
    return { 
      allData: {type: Array},
      loading: {type: Boolean}
    };
  }
  constructor() {
    super();
    
    this.url = location.origin+'/maildata.json'

    this.allData = [];
    this.loading=true; 
    const that = this; 
    that.loading=true; 
    fetch(that.url)
         .then((response) => {
            return response.json();
          }).then((data)=> {
            console.log(data);
            that.allData = data;
            that.loading=false;
          });
    window.setInterval(function(){
      that.loading=true; 
      fetch(that.url)
         .then((response) => {
            return response.json();
          }).then((data)=> {
            console.log(data);
            that.allData = data;
            that.loading=false;
          });

    },4000);
  }




  render(){
    /**
     * template at https://lit-element.polymer-project.org/guide/templates
     */

    function getHeader(h) {
      var result=[];
      for (const x in h) {
        result.push(html`
                <tr>
                  <td>${x}</td>
                  <td>${h[x]}</td>
                </tr>
        `);
        }
        return result; 
    };

    function getContent(c) {
      // return c.replace(/\</g, "&lt;").replace(/\n/g,"<br/>").trim();
      return c;
    }
    function getRaw(r) {
      // return r.replace(/\n/g,"<br/>").trim();
      return r; 
    }

    function listener(e) {
      var sel ="."+e.target.dataset["for"];
      var pp = e.target.closest(".panel");
      pp.querySelectorAll(".panel-body div").forEach(e=>e.classList.add("hide"));
      pp.querySelector(sel).classList.remove("hide");

      pp = e.target.parentNode;
      pp.querySelectorAll("button").forEach(e => e.classList.add("button-primary-outlined"));
      e.target.classList.remove("button-primary-outlined");

    }
    return html`
  <link rel="stylesheet" href="css/mustard-ui.min.css">

  <div class="container">
    <div style="position: fixed;top:0;width: 100%;margin: 0;" class="progress-bar striped animated ${this.loading ? '':'hide'}">
       <span class="progress-bar-red" style="width: 60%;"></span>
    </div>
      ${this.allData.length==0 ? 'No data':''}
      ${this.allData.map(e => 
          html`
              <section>
   
      <div class="panel">
        <div class="panel-head">
          <p class="panel-title">
            ${e.headers["SUBJECT"]}<br />
            <span>${e.headers["DATE"]}</span>
          </p>
        </div>
        <div class="panel-body">
          <div class="tab-header">
            <table>
              <thead>
                <tr>
                  <th>Key</th>
                  <th>Value </th>
                </tr>
              </thead>
              <tbody>
              ${getHeader(e.headers)}
              </tbody>
            </table>
          </div>

          <div class="tab-content hide">
            <pre>
                <code>${getContent(e.content)}</code>
            </pre>
          </div>
          <div class="tab-raw hide">
            <pre>
                <code>${getRaw(e.raw)}</code>
            </pre>
          </div>
        </div>
        <div class="panel-footer">
          <button @click=${listener} data-for="tab-header" class="button-primary">Header</button>
          <button @click=${listener} data-for="tab-content" class="button-primary  button-primary-outlined">Content</button>
          <button @click=${listener} data-for="tab-raw" class="button-primary button-primary-outlined">Raw</button>
        </div>
      </div>
    </section>

          `
      )}


  </div>
      

    `
  }
}
// Register the new element with the browser.
customElements.define('mail-server', MailServer);

