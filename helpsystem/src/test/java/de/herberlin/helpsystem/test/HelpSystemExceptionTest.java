package de.herberlin.helpsystem.test;

import junit.framework.TestCase;

/**
 * Dummy entry; no tests present.
 * 
 * @author Hans Joachim Herbertz
 * @created 16.03.2003
 */
public class HelpSystemExceptionTest extends TestCase {

	/**
	 * Constructor for HelpSystemExceptionTest.
	 * @param arg0
	 */
	public HelpSystemExceptionTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(HelpSystemExceptionTest.class);
	}
	
	public void testNix() {
		assertNotNull("This class must not be tested!");	
	}
}
