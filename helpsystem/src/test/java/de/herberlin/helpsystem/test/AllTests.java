package de.herberlin.helpsystem.test;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Test runner for all tests.
 * @author Hans Joachim Herbertz
 * @created 16.03.2003
 */
public class AllTests {
	
	public static final String HELP_LOCATION = "../bremsserverhelp/src/main/resources";
	public static final String HELP_JAR= "../bremsserverhelp/target/bremsserverhelp.jar";


	public static void main(String[] args) {
		//junit.swingui.TestRunner.run(AllTests.class);
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for de.herberlin.helpsystem.*");
		//$JUnit-BEGIN$
		//suite.addTest(new TestSuite(DefinedActionTest.class));
		suite.addTest(new TestSuite(HelpSystemExceptionTest.class));
		suite.addTest(new TestSuite(TreeRootTest.class));
		//$JUnit-END$
		return suite;
	}
}
