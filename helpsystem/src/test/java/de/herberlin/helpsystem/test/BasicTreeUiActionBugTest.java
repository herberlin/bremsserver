package de.herberlin.helpsystem.test;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.JTree;

import junit.framework.TestCase;

public class BasicTreeUiActionBugTest extends TestCase {

	public void testIt() throws Exception {

		JTree tree = new JTree();
		ActionMap actionMap = tree.getActionMap();
		Action action = null;
		action = actionMap.get("selectFirst");
		action.putValue(Action.SMALL_ICON,new
		      ImageIcon(getClass().getResource("/pic/Home16.gif")));
		action.putValue(Action.SHORT_DESCRIPTION, "Home");
		action.putValue(Action.NAME, "Home");
		System.out.println(System.getProperty("java.version"));
		System.out.println(action);
		System.out.println(action.getValue(Action.SMALL_ICON));
		System.out.println(action.getValue(Action.NAME));
		System.out.println(action.getValue(Action.SHORT_DESCRIPTION));

	}
}
