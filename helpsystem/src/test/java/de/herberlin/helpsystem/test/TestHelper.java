package de.herberlin.helpsystem.test;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import de.herberlin.helpsystem.TreeRoot;


/**
 * Little helpers for tests. Parsers for html files as
 * in AbstractTreeRoot class.
 *
 * @author Hans Joachim Herbertz
 * @created 23.03.2003
 */
public class TestHelper {

	class HTMLFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			if (new File(dir, name).isDirectory()) {
				return true;
			}
			if (name.endsWith(".html") || name.endsWith(".htm")) {
				return true;
			} else {
				return false;
			}
		}
	}
	public String getTitle(URL url) throws Exception  {

		BufferedReader in =new BufferedReader(new FileReader(url.getFile()));
		StringBuffer title=new StringBuffer();
		String buffer=null;
		while((buffer=in.readLine())!=null) {
			int startPos=Math.max(buffer.indexOf("<title>"),buffer.indexOf("<TITLE>"));	
			if (startPos>-1) {
				// start pos found
				int endPos=Math.max(buffer.indexOf("</title>"),buffer.indexOf("</TITLE>"));	
				if (endPos==-1) {
					title.append(buffer.substring(startPos+"<title>".length()));
				} else {
					title.append(buffer.substring(startPos+"<title>".length(),endPos));
				}
				while ( (endPos<0)	&& (buffer=in.readLine())!=null) {
					endPos=Math.max(buffer.indexOf("</title>"),buffer.indexOf("</TITLE>"));
					if (endPos==-1) {
						title.append(buffer);
					} else {
						title.append(buffer.substring(0,endPos));
					}
				}
				in.close();
				if (endPos<0) {
					// no end tag found
					return null;
				} else {
					return title.toString().trim();
				}		
			}
		}
		// no start tag found
		in.close();
		return null;	
	}
	
	public Map getIndexNameFileMap(File root) throws Exception  {
		Map map = new HashMap();
		File[] files=root.listFiles(new HTMLFilter());
		for (int i=0;i<files.length;i++) {
			if ( files[i].isFile()) {
					 map.put(TreeRoot.urlToIndex(files[i].toURL()),files[i].toURL());	
			} else if (files[i].isDirectory()) {
				map.putAll(getIndexNameFileMap(files[i]));	
			}
		}
		return map;
	}
	
}
