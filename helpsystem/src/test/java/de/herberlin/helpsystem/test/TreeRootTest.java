package de.herberlin.helpsystem.test;

import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import junit.framework.TestCase;
import de.herberlin.helpsystem.TreeNodeObject;
import de.herberlin.helpsystem.TreeRoot;

/**
 * Tests for AbstractTreeRoot parser class.
 * @author Hans Joachim Herbertz
 * @created 16.03.2003
 */
public class TreeRootTest extends TestCase {


	private TreeRoot testObj = null;
	private TestHelper helper = new TestHelper();
	/**
	 * Constructor for TreeRootTest.
	 * @param arg0
	 */
	public TreeRootTest(String arg0) {
		super(arg0);
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(TreeRootTest.class);
	}

	/**
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		testObj = new TreeRoot(AllTests.HELP_LOCATION);
	}

	public void testGetChildForIndex() throws Exception {
		testObj.parseTitles();
		Map map = helper.getIndexNameFileMap(TreeRoot.root);
		Iterator it = map.keySet().iterator();
		while (it.hasNext()) {
			String index = (String) it.next();
			TreeNodeObject nodeObj =
				(TreeNodeObject) testObj
					.getChildForIndex(index)
					.getUserObject();
			assertEquals(map.get(index), nodeObj.getUrlAsFile().toURL());
			assertEquals(
				helper.getTitle((URL) map.get(index)),
				nodeObj.getTitle());
		}
	}

	private boolean renameDir(boolean undo) {
		File helpdir = new File(System.getProperty("user.dir"), AllTests.HELP_LOCATION);
		File helpdirRenamed =
			new File(System.getProperty("user.dir"), AllTests.HELP_LOCATION + "_");
		if (undo) {
			return helpdirRenamed.renameTo(helpdir);
		} else {
			return helpdir.renameTo(helpdirRenamed);
		}

	}

	public void tearDown() {
		renameDir(true);
	}

	public void testGetChildForIndexZipFile() throws Exception {

		assertTrue("Rename fails", renameDir(false));
		testObj = new TreeRoot(AllTests.HELP_JAR);
		assertTrue(
			"Root is not jar file",
			TreeRoot.root.toString().endsWith(".jar"));

		ZipFile zip = new ZipFile(TreeRoot.root);
		Enumeration en = zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry et = (ZipEntry) en.nextElement();
			if (et.isDirectory())
				continue;
			String indexName = "/" + et.getName();
			if (indexName.lastIndexOf(".htm") > 0) {
				indexName =
					indexName.substring(0, indexName.lastIndexOf(".htm"));
			} else {
				continue;
			}
			// System.out.println(indexName);
			assertNotNull(
				indexName + " not found",
				testObj.getChildForIndex(indexName));
		}

	}
	public void testURLtoIndex() throws Exception {
		Map map = helper.getIndexNameFileMap(TreeRoot.root);
		Iterator it = map.keySet().iterator();
		while (it.hasNext()) {
			String index = (String) it.next();
			assertEquals(index, TreeRoot.urlToIndex((URL) map.get(index)));
		}

	}
	/**
	 * Test item does not exist on an unknown index name
	 * @throws Exception
	 */
	public void testTargetDoesNotExist() throws Exception {
		String key = "/idont/exist";
		assertNull(testObj.getChildForIndex(key));

	}

}
