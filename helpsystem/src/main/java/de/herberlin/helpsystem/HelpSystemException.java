package de.herberlin.helpsystem;

/**
 * Exception for Helpsystem
 * 
 * @author Hans Joachim Herbertz
 * @created 17.11.2002
 */
public class HelpSystemException extends Exception {

	/**
	 * Constructor for HelpSystemException.
	 */
	public HelpSystemException() {
		super();
	}

	/**
	 * Constructor for HelpSystemException.
	 * @param message
	 */
	public HelpSystemException(String message) {
		super(message);
	}

	/**
	 * Constructor for HelpSystemException.
	 * @param message
	 * @param cause
	 */
	public HelpSystemException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for HelpSystemException.
	 * @param cause
	 */
	public HelpSystemException(Throwable cause) {
		super(cause);
	}

}
