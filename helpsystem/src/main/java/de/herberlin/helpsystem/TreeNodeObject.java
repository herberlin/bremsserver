package de.herberlin.helpsystem;

import java.io.File;
import java.net.*;

/**
 * Simple data class holding tree node information;
 * A tree node is a File and a display name (html title).
 * 
 * @author Hans Joachim Herbertz
 * @created 17.11.2002
 */
public class TreeNodeObject {

	private URL url = null;
	/**
	 * @deprecated use url instead
	 */
	private String title = null;

	/**
	 * @param aFile
	 */
	public TreeNodeObject(File aFile) throws MalformedURLException {
		url = aFile.toURL();

	}
	public TreeNodeObject(URL aUrl) {
		this.url = aUrl;
	}
	public String toString() {
		if (title == null) {
			// find last matching directory name
			String[] segments = url.getPath().split("/");
			return segments[segments.length - 1];
		} else {
			return title;
		}
	}
	/**
	 * Returns the url as file.
	 * please note that it is not garantied that 
	 * that file exists!
	 * @return File
	 */
	public File getUrlAsFile() {
		return new File(url.getPath());
	}

	/**
	 * Returns the title.
	 * @return String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * @param title The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return
	 */
	public URL getUrl() {
		return url;
	}

	/**
	 * @param url
	 */
	public void setUrl(URL url) {
		this.url = url;
	}

}
