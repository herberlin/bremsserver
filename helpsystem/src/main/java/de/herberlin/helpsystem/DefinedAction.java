package de.herberlin.helpsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * Defines Actions for helpsystem that are not
 * already defined in the components itself.
 *
 * @author Hans Joachim Herbertz
 * @created 02.03.2003
 */
public class DefinedAction extends AbstractAction {

	private ActionListener listener=null;
	
	public DefinedAction(ActionListener actionListener) {
		listener=actionListener;
	}
	/**
	 * @see java.awt.event.ActionListener#actionPerformed(ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		listener.actionPerformed(e);
	}
	
	public static final short COPY_LINK_LOCATION=1;
	public static final short COPY_TREE_TEXT=2;
	public static final short HISTORY_BACK=3;
	
	public static  Action getAction(short actionId,ActionListener listener) {
		Action action=new DefinedAction(listener);
		try {
			switch (actionId) {
				case COPY_LINK_LOCATION:
					action.putValue(Action.SMALL_ICON,new ImageIcon(Object.class.getResource("/pic/WebComponentAdd16.gif")));
					action.putValue(Action.NAME,"copy-link");
					action.putValue(Action.SHORT_DESCRIPTION,"Copy link location");
					break;
							
				case COPY_TREE_TEXT:
					action.putValue(Action.SMALL_ICON,new ImageIcon(Object.class.getResource("/pic/Copy16.gif")));
					action.putValue(Action.NAME,"Copy");
					action.putValue(Action.SHORT_DESCRIPTION,"Copy to the clipboard");
					break;
							
				case HISTORY_BACK:
					action.putValue(Action.SMALL_ICON,new ImageIcon(Object.class.getResource("/pic/Back16.gif")));
					action.putValue(Action.NAME,"Back");
					action.putValue(Action.SHORT_DESCRIPTION,"History Back");
					break;
							
			}
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return action;
	}
}
