Simple Help System
==================

SimpleHelpSystem is a simple api I use to add html -  help to some applications. This is a very simple thing that parses a directory, zip or jar-file for some html -  files and transforms the directory structure into a tree structure.  Looks like this:

![Screenshot](./support/screenshot.png)

The html-page title tag is parsed and shown as a tree entry. If no title tag is present, the filename is shown. An index.html or index.htm file (if any) is used as entry for the directory entry. The other files are shown in alphabetical order.

The api's main entry point is the  

 `de.herberlin.helpsystem.SimpleHelpSystem`

This class extends javax.swing.JFrame so it should be easy to set up the SimpleHelpsystem with other applications. With the conctructor you give the location of your helpfiles. For example:

`JPanel panel=new SimpleHelpSystem(path-of-help);`

where path-of-help may be the root directory of your helpfiles or a jar or 
zip archive containing the helpfiles. 



For parsing the title tags may take a while it is recommendet to create help system after your application starts and hide it. You should not close the surrounding frame but just hide it to keep help system in memory.

To display a certain helppage, for example a page myhelp/support/contact.html call:

`SimpleHelpSystem#display("/support/contact");`

The key is the path to the corresponding file in the help system location without .html or .htm.

Commandline use
---------------
Thow helpsystem is designed to be embeddet into applications you may also
use it standalone.
By default, a directory named `help` it used as help root.

You may also give the location of you helpfiles at the commandline:

`java -jar helpsystem-1.1.0.jar myhelpfiles.zip`


Limitations:
------------
Help system does not work with JavaWebstart. I tried to make it work but did not succeed. I think the concept of parsing an existing directory of file at runtime is wrong for JavaWebstart.

Licence
-------
This software is distributed under the terms of the GNU Lesser General Public License. A copy of the licence is included with the sources. Copyright (c) 2001-2017 Hans Joachim Herbertz,  soft.herberlin.de, all rights reserved.

### Version 
#### 1.1.0
Moved latest to bitbucket

### Legacy Versions

####Version 0.1
 * Basic implementation.
 
#### Version 0.2
 * GUI Error handling.
 * UI Tests with jfcUnit
 * Open external links.
 * History for browsing. 
 * Browser. Link target preview on hover.
 * Enable / disable copy link location
 
#### Version 0.3
 * Launcher build for BasicJava Workshop
 
#### Version 0.4
 * Load from jar and zip
 * init with name instead of File
 * Tests updated; ui tests removed




