Project Bremsserver
===================

This is one of my first projects I started in 2001; I wanted to implement 
a webserver. 

Well, the thing developed over the time, and out came a nice litte tool for 
developing and testing. This was in 2004. Years later when developing with 
Liferay I thought it was helpfull to have an internal mailserver at the platform,
so the mail-server-portlet was born :) Nowadays there's also a port to android 
in preparation but not online yet.

So we have here: 

* [Bremsserver](./bremsserver) the ui and runnable server.
* [Server](./server) the server implementations shared between different platforms (compiles on android)
* [WwwUtil](./wwwutil) a standalone library for reading / writing web-streams (compiles on anddroid)
* [SimpleHelpSystem](./helpsystem) an embeddable and standalone ui for 
displaying html-helppages - like the old microsoft [WinHelp](https://de.wikipedia.org/wiki/Microsoft_Help)
* [Bremsserver Helpfiles](./bremsserverhelp)
* [Mail-Server-Portlet](./mail-server-portlet) for Liferay Portal 6.2

Please find available prebuilds at the [download page](https://bitbucket.org/herberlin/bremsserver/downloads/)

Please [contact me](http://soft.herberlin.de) if you want to buy support or develompent.



